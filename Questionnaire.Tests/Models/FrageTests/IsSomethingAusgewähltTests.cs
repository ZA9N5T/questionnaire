﻿using System.Collections.Generic;
using NUnit.Framework;
using Questionnaire.Models;

namespace Questionnaire.Tests.Models.FrageTests {
    [TestFixture]
    public class IsSomethingAusgewaehltTests {
        
        [Test]
        public void ListeMitEinerAusgewähltenAntwort() {
            var gegeben = new Frage() { Antworten = new List<Antwort>() {
                new Antwort() {Ausgewaehlt = false},
                new Antwort() {Ausgewaehlt = true}
            }};

            Assert.That(gegeben.IsSomethingAusgewählt, Is.EqualTo(true));
        }

        [Test]
        public void ListeMitZweiAusgewähltenAntworten()
        {
            var gegeben = new Frage()
            {
                Antworten = new List<Antwort>() {
                    new Antwort() {Ausgewaehlt = true},
                    new Antwort() {Ausgewaehlt = true}
                }
            };

            Assert.That(gegeben.IsSomethingAusgewählt, Is.EqualTo(true));
        }

        [Test]
        public void ListeMitKeinerAusgewähltenAntwort()
        {
            var gegeben = new Frage()
            {
                Antworten = new List<Antwort>() {
                    new Antwort() {Ausgewaehlt = false},
                    new Antwort() {Ausgewaehlt = false}
                }
            };

            Assert.That(gegeben.IsSomethingAusgewählt, Is.EqualTo(false));
        }
    }
}