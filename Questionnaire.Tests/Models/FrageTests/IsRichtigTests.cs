﻿using System.Collections.Generic;
using NUnit.Framework;
using Questionnaire.Models;

namespace Questionnaire.Tests.Models.FrageTests {
    [TestFixture]
    public class IsRichtigTests {
        
        [Test]
        public void ListeMitEinerRichtigenAntwort() {
            var gegeben = new Frage() {Antworten = new List<Antwort>() {
                new Antwort() {Ausgewaehlt = false, Korrekt = false},
                new Antwort() {Ausgewaehlt = true, Korrekt = true},
            }};

            Assert.That(gegeben.IsRichtig, Is.EqualTo(true));
        }

        [Test]
        public void ListeMitZweiRichtigenAntworten()
        {
            var gegeben = new Frage()
            {
                Antworten = new List<Antwort>() {
                    new Antwort() {Ausgewaehlt = true, Korrekt = true},
                    new Antwort() {Ausgewaehlt = true, Korrekt = true},
                }
            };

            Assert.That(gegeben.IsRichtig, Is.EqualTo(true));
        }

        [Test]
        public void ListeMitEinerFalschenAntwort()
        {
            var gegeben = new Frage()
            {
                Antworten = new List<Antwort>() {
                    new Antwort() {Ausgewaehlt = false, Korrekt = false},
                    new Antwort() {Ausgewaehlt = true, Korrekt = false},
                }
            };

            Assert.That(gegeben.IsRichtig, Is.EqualTo(false));
        }

        [Test]
        public void ListeMitZweiFalschenAntworten()
        {
            var gegeben = new Frage()
            {
                Antworten = new List<Antwort>() {
                    new Antwort() {Ausgewaehlt = true, Korrekt = false},
                    new Antwort() {Ausgewaehlt = true, Korrekt = false},
                }
            };

            Assert.That(gegeben.IsRichtig, Is.EqualTo(false));
        }


    }
}