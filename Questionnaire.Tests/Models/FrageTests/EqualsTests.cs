﻿using NUnit.Framework;
using Questionnaire.Models;

namespace Questionnaire.Tests.Models.FrageTests {
    [TestFixture]
    public class EqualsTests {
        [Test]
        public void VergleichMitSichSelbst() {
            var frage = new Frage();

            // ReSharper disable EqualExpressionComparison
            Assert.That(frage.Equals(frage), Is.True);
            // ReSharper restore EqualExpressionComparison
        }

        [Test]
        public void VergleichMitAndererFrageMitGleichenValues()
        {
            var frage = new Frage();
            var aFrage = new Frage();

            Assert.That(frage.Equals(aFrage), Is.True);
        }

        [Test]
        public void VergleichMitNull()
        {
            var frage = new Frage();

            Assert.That(frage.Equals(null), Is.False);
        }

        [Test]
        public void VergleichMitAnderemObjekt()
        {
            var frage = new Frage();
            var antwort = new Antwort();

            // ReSharper disable SuspiciousTypeConversion.Global
            Assert.That(frage.Equals(antwort), Is.False);
            // ReSharper restore SuspiciousTypeConversion.Global
        }

    }
}