﻿using NUnit.Framework;
using Questionnaire.Models;

namespace Questionnaire.Tests.Models.FrageTests {
    [TestFixture]
    public class GetHashCodeTests {
        [Test]
        public void VergleichMitSichSelbst() {
            var frage = new Frage();
            Assert.That(frage.GetHashCode(), Is.EqualTo(frage.GetHashCode()));
        }

    }
}