﻿using NUnit.Framework;
using Questionnaire.Models;

namespace Questionnaire.Tests.Models.AntwortTests {
    [TestFixture]
    public class GetHashCodeTests {
        [Test]
        public void VergleichMitSichSelbst()
        {
            var frage = new Antwort();
            Assert.That(frage.GetHashCode(), Is.EqualTo(frage.GetHashCode()));
        }
    }
}