﻿using NUnit.Framework;
using Questionnaire.Models;

namespace Questionnaire.Tests.Models.AntwortTests {
    [TestFixture]
    public class EqualsTests {
        [Test]
        public void VergleichMitSichSelbst()
        {
            var antwort = new Antwort();

            // ReSharper disable EqualExpressionComparison
            Assert.That(antwort.Equals(antwort), Is.True);
            // ReSharper restore EqualExpressionComparison
        }

        [Test]
        public void VergleichMitAndererFrageMitGleichenValues()
        {
            var antwort = new Antwort();
            var aAntwort = new Antwort();

            Assert.That(antwort.Equals(aAntwort), Is.True);
        }

        [Test]
        public void VergleichMitNull()
        {
            var antwort = new Antwort();

            Assert.That(antwort.Equals(null), Is.False);
        }

        [Test]
        public void VergleichMitAnderemObjekt()
        {
            var antwort = new Antwort();
            var frage = new Frage();

            // ReSharper disable SuspiciousTypeConversion.Global
            Assert.That(antwort.Equals(frage), Is.False);
            // ReSharper restore SuspiciousTypeConversion.Global
        }
    }
}