﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Questionnaire.Models;
using Questionnaire.Services;

namespace Questionnaire.Tests.Services.MessageServiceTests {
    [TestFixture]
    public class GenerateErgebnisTextTests {
        [Test]
        public void ListeMitEinerRichtigenFrage() {
            var gegeben = new List<Frage> {
                new Frage() {
                    Text = "Frage 1",
                    Antworten = new List<Antwort>() {
                        new Antwort() {Text = "Antwort 1", Ausgewaehlt = true, Korrekt = true}
                    }

                }
            };
            var erwartet = $"Frage 1{Environment.NewLine}{Environment.NewLine}\tYour answer 'Antwort 1' is correct{Environment.NewLine}{Environment.NewLine}";

            var ergebnis = MessageService.GenerateErgebnisText(gegeben);
            Assert.That(ergebnis, Is.EqualTo(erwartet));
        }

        [Test]
        public void ListeMitZweiRichtigenFragen()
        {
            var gegeben = new List<Frage> {
                new Frage() {
                    Text = "Frage 1",
                    Antworten = new List<Antwort>() {
                        new Antwort() {Text = "Antwort 1", Ausgewaehlt = true, Korrekt = true}
                    }

                },
                new Frage() {
                    Text = "Frage 2",
                    Antworten = new List<Antwort>() {
                        new Antwort() {Text = "Antwort 1", Ausgewaehlt = true, Korrekt = true}
                    }

                }
            };
            var erwartet = $"Frage 1{Environment.NewLine}{Environment.NewLine}\tYour answer 'Antwort 1' is correct{Environment.NewLine}{Environment.NewLine}Frage 2{Environment.NewLine}{Environment.NewLine}\tYour answer 'Antwort 1' is correct{Environment.NewLine}{Environment.NewLine}";

            var ergebnis = MessageService.GenerateErgebnisText(gegeben);
            Assert.That(ergebnis, Is.EqualTo(erwartet));
        }

        [Test]
        public void ListeMitEinerRichtigenUndEinerFalschenFrage()
        {
            var gegeben = new List<Frage> {
                new Frage() {
                    Text = "Frage 1",
                    Antworten = new List<Antwort>() {
                        new Antwort() {Text = "Antwort 1", Ausgewaehlt = true, Korrekt = true}
                    }

                },
                new Frage() {
                    Text = "Frage 2",
                    Antworten = new List<Antwort>() {
                        new Antwort() {Text = "Antwort 1", Ausgewaehlt = true, Korrekt = false},
                        new Antwort() {Text = "Antwort 2", Ausgewaehlt = false, Korrekt = true}
                    }
                }
            };
            var erwartet = $"Frage 1{Environment.NewLine}{Environment.NewLine}\tYour answer 'Antwort 1' is correct{Environment.NewLine}{Environment.NewLine}Frage 2{Environment.NewLine}{Environment.NewLine}\tYour answer 'Antwort 1' is wrong{Environment.NewLine}\tThe answer 'Antwort 2' should be checked{Environment.NewLine}{Environment.NewLine}";

            var ergebnis = MessageService.GenerateErgebnisText(gegeben);
            Assert.That(ergebnis, Is.EqualTo(erwartet));
        }
    }
}