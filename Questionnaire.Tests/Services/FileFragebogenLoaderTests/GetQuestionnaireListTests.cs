﻿using System.IO;
using NUnit.Framework;
using Questionnaire.Services;

namespace Questionnaire.Tests.Services.FileFragebogenLoaderTests {
    [TestFixture]
    public class GetQuestionnaireListTests {
        [SetUp]
        public void Setup() {
            var questionnairesPath = Path.Combine(TestContext.CurrentContext.WorkDirectory,
                TestContext.CurrentContext.Test.Name);

            Directory.CreateDirectory(questionnairesPath);
            Directory.SetCurrentDirectory(questionnairesPath);
        }
        
        [Test]
        public void OrdnerMitEinerDatei() {
            var sut = new FileFragebogenLoader();

            GenerateTestFiles(1);

            Assert.That(sut.GetQuestionnaireList().Count, Is.EqualTo(1));
        }

        [Test]
        public void OrdnerMitZweiDateien()
        {
            var sut = new FileFragebogenLoader();

            GenerateTestFiles(2);

            Assert.That(sut.GetQuestionnaireList().Count, Is.EqualTo(2));
        }

        [Test]
        public void OrdnerMitKeinerDatei()
        {
            var sut = new FileFragebogenLoader();
            
            Assert.That(sut.GetQuestionnaireList().Count, Is.EqualTo(0));
        }

        private void GenerateTestFiles(int amount) {
            for (int i = 0; i < amount; i++) {
                File.Create(Path.ChangeExtension(i.ToString(), ".txt"));
            }
        }
    }
}