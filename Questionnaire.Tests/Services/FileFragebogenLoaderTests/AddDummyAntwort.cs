﻿using System.Collections.Generic;
using NUnit.Framework;
using Questionnaire.Models;
using Questionnaire.Services;

namespace Questionnaire.Tests.Services.FileFragebogenLoaderTests {
    [TestFixture]
    public class AddDummyAntwort {
        
        [Test]
        public void LehreListe() {
            var gegeben = new List<Frage>(){ new Frage() {Antworten = new List<Antwort>()}};
            var erwartet = new List<Frage>() { new Frage() {Antworten = new List<Antwort>() {new Antwort() {Text = Antwort.DONT_KNOW_TEXT}}}};

            Assert.That(FileFragebogenLoader.AddDummyAntwort(gegeben), Is.EqualTo(erwartet));
        }

        [Test]
        public void ListeMitEinerFrage()
        {
            var gegeben = new List<Frage>() { new Frage() { Antworten = new List<Antwort>() {new Antwort()} } };
            var erwartet = new List<Frage>() { new Frage() { Antworten = new List<Antwort>() { new Antwort(), new Antwort() { Text = Antwort.DONT_KNOW_TEXT } } } };

            Assert.That(FileFragebogenLoader.AddDummyAntwort(gegeben), Is.EqualTo(erwartet));
        }

    }
}