﻿using System.Collections.Generic;
using NUnit.Framework;
using Questionnaire.Models;
using Questionnaire.Services;

namespace Questionnaire.Tests.Services.FileFragebogenLoaderTests {
    [TestFixture]
    public class CreateFragebogenListeTests {
        
        [Test]
        public void ListeMitZweiFragenUndJeZweiAntworten() {
            var gegeben = new[] {"?Wie geht es dir", "Test", "*Test2", "?Hallo Welt", "Test", "*Test2"};
            var erwartet = new List<Frage> {
                new Frage() {Text = "Wie geht es dir?", Antworten = new List<Antwort>() {
                    new Antwort(){Text = "Test", Ausgewaehlt = false, Korrekt = false},
                    new Antwort(){Text = "Test2", Ausgewaehlt = false, Korrekt = true}
                }},
                new Frage() {Text = "Hallo Welt?", Antworten = new List<Antwort>() {
                    new Antwort(){Text = "Test", Ausgewaehlt = false, Korrekt = false},
                    new Antwort(){Text = "Test2", Ausgewaehlt = false, Korrekt = true}
                }},
            };

            Assert.That(FileFragebogenLoader.CreateFragebogenListe(gegeben), Is.EqualTo(erwartet));
        }

        [Test]
        public void ListeMitZweiFragenEineMitOhneAntworten()
        {
            var gegeben = new[] { "?Wie geht es dir", "?Hallo Welt", "Test", "*Test2" };
            var erwartet = new List<Frage> {
                new Frage() {Text = "Wie geht es dir?", Antworten = new List<Antwort>()},
                new Frage() {Text = "Hallo Welt?", Antworten = new List<Antwort>() {
                    new Antwort(){Text = "Test", Ausgewaehlt = false, Korrekt = false},
                    new Antwort(){Text = "Test2", Ausgewaehlt = false, Korrekt = true}
                }},
            };

            Assert.That(FileFragebogenLoader.CreateFragebogenListe(gegeben), Is.EqualTo(erwartet));
        }

        [Test]
        public void ListeMitZweiFragenUndMehrerenKorrektenAntworten()
        {
            var gegeben = new[] { "?Wie geht es dir", "Test", "*Test2", "*Test3", "?Hallo Welt", "Test", "*Test2" };
            var erwartet = new List<Frage> {
                new Frage() {Text = "Wie geht es dir?", Antworten = new List<Antwort>() {
                    new Antwort(){Text = "Test", Ausgewaehlt = false, Korrekt = false},
                    new Antwort(){Text = "Test2", Ausgewaehlt = false, Korrekt = true},
                    new Antwort(){Text = "Test3", Ausgewaehlt = false, Korrekt = true}
                }},
                new Frage() {Text = "Hallo Welt?", Antworten = new List<Antwort>() {
                    new Antwort(){Text = "Test", Ausgewaehlt = false, Korrekt = false},
                    new Antwort(){Text = "Test2", Ausgewaehlt = false, Korrekt = true}
                }},
            };

            Assert.That(FileFragebogenLoader.CreateFragebogenListe(gegeben), Is.EqualTo(erwartet));
        }

        [Test]
        public void ListeOhneFrage()
        {
            var gegeben = new[] { "" };
            var erwartet = new List<Frage> ();

            Assert.That(FileFragebogenLoader.CreateFragebogenListe(gegeben), Is.EqualTo(erwartet));
        }

    }
}