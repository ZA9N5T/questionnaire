﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using Questionnaire.Models;
using Questionnaire.Services;

namespace Questionnaire.Tests.Services.FileFragebogenLoaderTests {
    [TestFixture]
    public class GetFragebogenTests {

        [SetUp]
        public void Setup()
        {
            var questionnairesPath = Path.Combine(TestContext.CurrentContext.WorkDirectory,
                TestContext.CurrentContext.Test.Name);

            Directory.CreateDirectory(questionnairesPath);
            Directory.SetCurrentDirectory(questionnairesPath);
        }

        [Test]
        public void DateiMitEinerFrageUndZweiAntworten() {
            var sut = new FileFragebogenLoader();
            var path = Path.Combine(Directory.GetCurrentDirectory(), Path.ChangeExtension("test", ".txt"));
            var text = $"?Frage 1{Environment.NewLine}Antwort 1{Environment.NewLine}Antwort 2";
            var erwartet = new List<Frage> {
                new Frage() {
                    Text = "Frage 1?",
                    Antworten = new List<Antwort>() {
                        new Antwort(){Text = "Antwort 1"},
                        new Antwort(){Text = "Antwort 2"},
                        new Antwort(){Text = "Dont Know"}
                    }
                   
                }
            };
            
            File.WriteAllText(path, text);

            Assert.That(sut.GetFragebogen(path), Is.EqualTo(erwartet));
        }

    }
}