﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using Questionnaire.Models;
using Questionnaire.Services;
using Questionnaire.ViewModels;

namespace Questionnaire.Tests.ViewModels.QuestionnaireViewModelTests {
    [TestFixture]
    public class CanErgebnisExecuteTests {
        [Test]
        public void ListeMit2FragenAlleHabenEtwasAusgewählt() {
            var fragebogenLoader = new Mock<IFragebogenLoader>();
            var liste = new List<Frage>() {
                new Frage(){Text = "", Antworten = new List<Antwort>(){
                    new Antwort(){Text = "", Ausgewaehlt = true, Korrekt = true}
                }},
                new Frage(){Text = "", Antworten = new List<Antwort>(){
                    new Antwort(){Text = "", Ausgewaehlt = true, Korrekt = true}
                }},
            };
            fragebogenLoader.Setup(loader => loader.GetQuestionnaireList())
                .Returns(new List<QuestionnaireFile>() {new QuestionnaireFile() {Name = "test", Ausgewählt = true}});
            fragebogenLoader.Setup(loader => loader.GetFragebogen(It.IsAny<string>())).Returns(liste);
            var sut = new QuestionnaireViewModel(fragebogenLoader.Object);

            sut.LadeFragebogen();

            Assert.That(sut.CanErgebnisExecute(null), Is.EqualTo(true));
        }

        [Test]
        public void ListeMit2FragenEineHatEtwasAusgewählt()
        {
            var fragebogenLoader = new Mock<IFragebogenLoader>();
            var liste = new List<Frage>() {
                new Frage(){Text = "", Antworten = new List<Antwort>(){
                    new Antwort(){Text = "", Ausgewaehlt = true, Korrekt = true}
                }},
                new Frage(){Text = "", Antworten = new List<Antwort>(){
                    new Antwort(){Text = "", Ausgewaehlt = false, Korrekt = true}
                }}
            };
            fragebogenLoader.Setup(loader => loader.GetQuestionnaireList())
                .Returns(new List<QuestionnaireFile>() { new QuestionnaireFile() { Name = "test", Ausgewählt = true } });
            fragebogenLoader.Setup(loader => loader.GetFragebogen(It.IsAny<string>())).Returns(liste);
            var sut = new QuestionnaireViewModel(fragebogenLoader.Object);

            sut.LadeFragebogen();

            Assert.That(sut.CanErgebnisExecute(null), Is.EqualTo(false));
        }

        [Test]
        public void ListeMit2FragenKeineHatEtwasAusgewählt()
        {
            var fragebogenLoader = new Mock<IFragebogenLoader>();
            var liste = new List<Frage>() {
                new Frage(){Text = "", Antworten = new List<Antwort>(){
                    new Antwort(){Text = "", Ausgewaehlt = false, Korrekt = true}
                }},
                new Frage(){Text = "", Antworten = new List<Antwort>(){
                    new Antwort(){Text = "", Ausgewaehlt = false, Korrekt = true}
                }}
            };
            fragebogenLoader.Setup(loader => loader.GetQuestionnaireList())
                .Returns(new List<QuestionnaireFile>() { new QuestionnaireFile() { Name = "test", Ausgewählt = true } });
            fragebogenLoader.Setup(loader => loader.GetFragebogen(It.IsAny<string>())).Returns(liste);
            var sut = new QuestionnaireViewModel(fragebogenLoader.Object);

            sut.LadeFragebogen();

            Assert.That(sut.CanErgebnisExecute(null), Is.EqualTo(false));
        }

    }
}