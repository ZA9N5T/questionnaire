﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using Questionnaire.Models;
using Questionnaire.Services;
using Questionnaire.ViewModels;

namespace Questionnaire.Tests.ViewModels.QuestionnaireViewModelTests {
    [TestFixture]
    public class CanStartQuizExecuteTests {
        [Test]
        public void ListeMitEinemQuestionnaireKeinesAusgewählt() {
            var fragebogenLoader = new Mock<IFragebogenLoader>();
            fragebogenLoader.Setup(loader => loader.GetQuestionnaireList())
                .Returns(new List<QuestionnaireFile>() { new QuestionnaireFile() { Name = "test", Ausgewählt = false } });
            var sut = new QuestionnaireViewModel(fragebogenLoader.Object);

            Assert.That(sut.CanStartQuizExecute(null), Is.EqualTo(false));
        }

        [Test]
        public void ListeMitEinemQuestionnaireEinesAusgewählt()
        {
            var fragebogenLoader = new Mock<IFragebogenLoader>();
            fragebogenLoader.Setup(loader => loader.GetQuestionnaireList())
                .Returns(new List<QuestionnaireFile>() { new QuestionnaireFile() { Name = "test", Ausgewählt = true } });
            var sut = new QuestionnaireViewModel(fragebogenLoader.Object);

            Assert.That(sut.CanStartQuizExecute(null), Is.EqualTo(true));
        }

    }
}