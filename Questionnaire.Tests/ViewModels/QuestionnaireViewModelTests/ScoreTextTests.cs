﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using Questionnaire.Models;
using Questionnaire.Services;
using Questionnaire.ViewModels;

namespace Questionnaire.Tests.ViewModels.QuestionnaireViewModelTests {
    [TestFixture]
    public class ScoreTextTests {
        [Test]
        public void ListeMitZweiFragenAlleRichtig() {
            var gegeben = new List<Frage> {

                new Frage() {Text = "Frage 1", Antworten = new List<Antwort>() {
                    new Antwort() {Text = "Antwort 1", Ausgewaehlt = true, Korrekt = true},
                    new Antwort() {Text = "Antwort 2", Ausgewaehlt = false, Korrekt = false},
                }},
                new Frage() {Text = "Frage 2", Antworten = new List<Antwort>() {
                    new Antwort() {Text = "Antwort 1", Ausgewaehlt = false, Korrekt = false},
                    new Antwort() {Text = "Antwort 2", Ausgewaehlt = true, Korrekt = true},
                }},
            };
            var fragebogenLoader = new Mock<IFragebogenLoader>();
            fragebogenLoader.Setup(loader => loader.GetQuestionnaireList())
                .Returns(new List<QuestionnaireFile>() { new QuestionnaireFile() { Name = "test", Ausgewählt = true } });
            fragebogenLoader.Setup(loader => loader.GetFragebogen(It.IsAny<string>())).Returns(gegeben);
            var sut = new QuestionnaireViewModel(fragebogenLoader.Object);

            sut.LadeFragebogen();

            Assert.That(sut.ScoreText, Is.EqualTo(QuestionnaireViewModel.GenerateScoreText(QuestionnaireViewModel.CountRichtigeFragen(sut.Fragebogen), sut.Fragebogen.Count)));
        }

        [Test]
        public void ListeMitZweiFragenEineRichtig()
        {
            var gegeben = new List<Frage> {

                new Frage() {Text = "Frage 1", Antworten = new List<Antwort>() {
                    new Antwort() {Text = "Antwort 1", Ausgewaehlt = true, Korrekt = true},
                    new Antwort() {Text = "Antwort 2", Ausgewaehlt = false, Korrekt = false},
                }},
                new Frage() {Text = "Frage 2", Antworten = new List<Antwort>() {
                    new Antwort() {Text = "Antwort 1", Ausgewaehlt = true, Korrekt = false},
                    new Antwort() {Text = "Antwort 2", Ausgewaehlt = false, Korrekt = true},
                }},
            };
            var fragebogenLoader = new Mock<IFragebogenLoader>();
            fragebogenLoader.Setup(loader => loader.GetQuestionnaireList())
                .Returns(new List<QuestionnaireFile>() { new QuestionnaireFile() { Name = "test", Ausgewählt = true } });
            fragebogenLoader.Setup(loader => loader.GetFragebogen(It.IsAny<string>())).Returns(gegeben);
            var sut = new QuestionnaireViewModel(fragebogenLoader.Object);

            sut.LadeFragebogen();

            Assert.That(sut.ScoreText, Is.EqualTo(QuestionnaireViewModel.GenerateScoreText(QuestionnaireViewModel.CountRichtigeFragen(sut.Fragebogen), sut.Fragebogen.Count)));
        }

    }
}