﻿using System.Collections.Generic;
using NUnit.Framework;
using Questionnaire.Models;
using Questionnaire.ViewModels;

namespace Questionnaire.Tests.ViewModels.QuestionnaireViewModelTests {
    [TestFixture]
    public class CountRichtigeFragenTests {

        [Test]
        public void ListeMitEinerRichtigenFrage() {
            var gegeben = new List<Frage> {
                new Frage() {
                    Antworten = new List<Antwort>() {
                        new Antwort() {Ausgewaehlt = false, Korrekt = false}
                    }
                }
            };

            Assert.That(QuestionnaireViewModel.CountRichtigeFragen(gegeben), Is.EqualTo(1));
        }

        [Test]
        public void ListeMitEinerRichtigenFrageUndZweiFragen() {
            var gegeben = new List<Frage> {
                new Frage() {
                    Antworten = new List<Antwort>() {
                        new Antwort() {Ausgewaehlt = false, Korrekt = false},
                        new Antwort() {Ausgewaehlt = true, Korrekt = true},
                    }
                }
            };

            Assert.That(QuestionnaireViewModel.CountRichtigeFragen(gegeben), Is.EqualTo(1));
        }

        [Test]
        public void ListeMitZweiRichtigenFragen()
        {
            var gegeben = new List<Frage> {
                new Frage(){Antworten = new List<Antwort>() {
                    new Antwort(){Ausgewaehlt = false, Korrekt = false},
                    new Antwort(){Ausgewaehlt = true, Korrekt = true},
                }},
                new Frage(){Antworten = new List<Antwort>() {
                    new Antwort(){Ausgewaehlt = false, Korrekt = false},
                    new Antwort(){Ausgewaehlt = true, Korrekt = true},
                }},
            };

            Assert.That(QuestionnaireViewModel.CountRichtigeFragen(gegeben), Is.EqualTo(2));
        }

        [Test]
        public void ListeMitEinerRichtigenUndEinerFalschenFrage()
        {
            var gegeben = new List<Frage> {
                new Frage(){Antworten = new List<Antwort>() {
                    new Antwort(){Ausgewaehlt = false, Korrekt = false},
                    new Antwort(){Ausgewaehlt = true, Korrekt = true},
                }},
                new Frage(){Antworten = new List<Antwort>() {
                    new Antwort(){Ausgewaehlt = true, Korrekt = false},
                    new Antwort(){Ausgewaehlt = true, Korrekt = true},
                }}
            };

            Assert.That(QuestionnaireViewModel.CountRichtigeFragen(gegeben), Is.EqualTo(1));
        }

        [Test]
        public void ListeMitZweiFalschenFragen()
        {
            var gegeben = new List<Frage> {
                new Frage(){Antworten = new List<Antwort>() {
                    new Antwort(){Ausgewaehlt = false, Korrekt = false},
                    new Antwort(){Ausgewaehlt = false, Korrekt = true},
                }},
                new Frage(){Antworten = new List<Antwort>() {
                    new Antwort(){Ausgewaehlt = false, Korrekt = false},
                    new Antwort(){Ausgewaehlt = false, Korrekt = true},
                }},
            };

            Assert.That(QuestionnaireViewModel.CountRichtigeFragen(gegeben), Is.EqualTo(0));
        }

        [Test]
        public void ListeMitEinerFalschenFrage()
        {
            var gegeben = new List<Frage> {
                new Frage(){Antworten = new List<Antwort>() {
                    new Antwort(){Ausgewaehlt = false, Korrekt = false},
                    new Antwort(){Ausgewaehlt = false, Korrekt = true},
                }}
            };

            Assert.That(QuestionnaireViewModel.CountRichtigeFragen(gegeben), Is.EqualTo(0));
        }

    }
}