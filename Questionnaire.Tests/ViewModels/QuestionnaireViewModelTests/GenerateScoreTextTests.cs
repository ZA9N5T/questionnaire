﻿using NUnit.Framework;
using Questionnaire.ViewModels;

namespace Questionnaire.Tests.ViewModels.QuestionnaireViewModelTests {
    [TestFixture]
    public class GenerateScoreTextTests {

        [Test]
        public void EineVonZweiRichtig() {
            var erwartet = $"1 out of 2 questions answered correctly ({1.0 / 2.0:P})";

            Assert.That(QuestionnaireViewModel.GenerateScoreText(1,2), Is.EqualTo(erwartet));
        }

        [Test]
        public void NullVonZweiRichtig()
        {
            var erwartet = $"0 out of 2 questions answered correctly ({0.0 / 2.0:P})";

            Assert.That(QuestionnaireViewModel.GenerateScoreText(0, 2), Is.EqualTo(erwartet));
        }

        [Test]
        public void ZweiVonZweiRichtig()
        {
            var erwartet = $"2 out of 2 questions answered correctly ({2.0 / 2.0:P})";

            Assert.That(QuestionnaireViewModel.GenerateScoreText(2, 2), Is.EqualTo(erwartet));
        }

        [Test]
        public void ElfVonZweiRichtig()
        {
            var erwartet = $"11 out of 2 questions answered correctly ({11.0 / 2.0:P})";

            Assert.That(QuestionnaireViewModel.GenerateScoreText(11, 2), Is.EqualTo(erwartet));
        }

        [Test]
        public void NullVonNullRichtig()
        {
            var erwartet = $"0 out of 0 questions answered correctly ({0.0 / 0.0:P})";

            Assert.That(QuestionnaireViewModel.GenerateScoreText(0, 0), Is.EqualTo(erwartet));
        }


    }
}