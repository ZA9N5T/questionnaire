﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows.Input;
using Prism.Mvvm;
using Questionnaire.Models;
using Questionnaire.Services;
using Questionnaire.ViewModels.Commands;
using Questionnaire.Views;

namespace Questionnaire.ViewModels
{
    public class QuestionnaireViewModel : BindableBase {
        public List<Frage> Fragebogen { get; private set; }
        public List<QuestionnaireFile> Questionnaires { get; }

        private readonly IFragebogenLoader _fragebogenLoader;

        public string AntwortText => MessageService.GenerateErgebnisText(Fragebogen);

        public string ScoreText => GenerateScoreText(CountRichtigeFragen(Fragebogen), Fragebogen.Count);
        
        public ICommand ErgebisCommand { get; }
        public ICommand StartQuizCommand { get; }

        public QuestionnaireViewModel(IFragebogenLoader fragebogenLoader) {
            _fragebogenLoader = fragebogenLoader;
            Questionnaires = fragebogenLoader.GetQuestionnaireList();

            StartQuizCommand = new ActionCommand(HandleStartQuizExecuted, CanStartQuizExecute);
            ErgebisCommand = new ActionCommand(HandleErgebnisExecuted, CanErgebnisExecute);
        }

        internal bool CanStartQuizExecute(object arg) {
            return Questionnaires.Any(item => item.Ausgewählt);
        }

        [ExcludeFromCodeCoverage]
        private void HandleStartQuizExecuted(object obj) {
            LadeFragebogen();
            (new FragebogenWindow()).ShowDialog();
        }

        internal void LadeFragebogen() {
            Fragebogen = _fragebogenLoader.GetFragebogen(Questionnaires.FirstOrDefault(item => item.Ausgewählt)?.Name);
        }

        public bool CanErgebnisExecute(object x) {
            return Fragebogen.All(frage => frage.IsSomethingAusgewählt());
        }

        [ExcludeFromCodeCoverage]
        private static void HandleErgebnisExecuted(object x) {
            (new ScoreWindow()).ShowDialog();
        }

        internal static int CountRichtigeFragen(IEnumerable<Frage> fragebogen) {
            return fragebogen.Count(frage => frage.IsRichtig());
        }

        internal static string GenerateScoreText(double richtigeFragen, double alleFragen) {
            return $"{richtigeFragen} out of {alleFragen} questions answered correctly ({richtigeFragen / alleFragen:P})";
        }
    }
}
