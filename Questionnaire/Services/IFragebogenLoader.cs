﻿using System.Collections.Generic;
using Questionnaire.Models;

namespace Questionnaire.Services {
    public interface IFragebogenLoader {
        List<Frage> GetFragebogen(string name);
        List<QuestionnaireFile> GetQuestionnaireList();
    }
}