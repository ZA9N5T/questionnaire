﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Questionnaire.Models;

namespace Questionnaire.Services
{
    public static class MessageService {
        private static readonly string LineSeperator = Environment.NewLine + Environment.NewLine;

        public static string GenerateErgebnisText(List<Frage> fragebogen) {
            var text = string.Empty;
            fragebogen.ForEach(frage => {
                text += frage.Text + Environment.NewLine;
                frage.Antworten.ForEach(antwort => {
                    if (antwort.Ausgewaehlt && antwort.Korrekt)
                    {
                        text += $"{Environment.NewLine}\tYour answer '{antwort.Text}' is correct";
                    }
                    else if (!antwort.Ausgewaehlt && antwort.Korrekt)
                    {
                        text += $"{Environment.NewLine}\tThe answer '{antwort.Text}' should be checked";
                    }
                    else if (antwort.Ausgewaehlt && !antwort.Korrekt)
                    {
                        text += $"{Environment.NewLine}\tYour answer '{antwort.Text}' is wrong";
                    }
                });
                text += LineSeperator;
            });
            return text;
        }
    }
}
