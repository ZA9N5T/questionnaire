﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Questionnaire.Models;
using Unity.Interception.Utilities;

namespace Questionnaire.Services {
    public class FileFragebogenLoader : IFragebogenLoader {
        private const char _QUESTION_IDENTIFIER = '?'; 
        private const char _RIGHT_ANSWER_IDENTIFIER = '*';
        private const string _FILE_TYPE = ".txt";

        public List<Frage> GetFragebogen(string name) {
            var filepath = Path.ChangeExtension(name, _FILE_TYPE);
            var fileLines = ReadAllLines(filepath);
            return CreateFragebogenListe(fileLines);
        }

        public static List<Frage> CreateFragebogenListe(string[] fileLines)
        {
            var fragebogen = new List<Frage>();
            Frage frage = null;
            
            fileLines.ForEach(item =>
            {
                if (item.StartsWith(_QUESTION_IDENTIFIER.ToString())) {
                    frage = new Frage() {Text = String.Concat(item.TrimStart(_QUESTION_IDENTIFIER), '?')};
                    fragebogen.Add(frage);
                    frage.Antworten = new List<Antwort>();
                }
                else {
                    frage?.Antworten.Add(new Antwort() {Text = item.TrimStart(_RIGHT_ANSWER_IDENTIFIER), Korrekt = item.StartsWith(_RIGHT_ANSWER_IDENTIFIER.ToString()) });
                }
            });
            fragebogen = AddDummyAntwort(fragebogen);
            return fragebogen;
        }

        internal static List<Frage> AddDummyAntwort(List<Frage> fragebogen) {
            fragebogen.ForEach(frage => {
                frage.Antworten.Add(new Antwort(){Text = Antwort.DONT_KNOW_TEXT, Korrekt = false});
            });
            return fragebogen;
        }

        private string[] ReadAllLines(string pfad) {
            return File.ReadAllLines(pfad);
        }

        public List<QuestionnaireFile> GetQuestionnaireList() {
            var liste = new List<QuestionnaireFile>();
            Directory.GetFiles(Environment.CurrentDirectory).Where(file => file.EndsWith(_FILE_TYPE))
                .ForEach(file => {
                    var filename = Path.ChangeExtension(Path.GetFileName(file), "");
                    liste.Add(new QuestionnaireFile(){Name = filename});
                });
            return liste;
        }
    }
}