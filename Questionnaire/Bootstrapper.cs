﻿using System.Windows;
using Prism.Mvvm;
using Prism.Unity;
using Questionnaire.Services;
using Questionnaire.ViewModels;
using Questionnaire.Views;

namespace Questionnaire {
    public class Bootstrapper : UnityBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            return Container.TryResolve<MainWindow>();
        }

        protected override void InitializeShell()
        {
            Application.Current.MainWindow?.Show();
        }
        
        protected override void ConfigureContainer() {
            base.ConfigureContainer();

            RegisterTypeIfMissing(typeof(IFragebogenLoader), typeof(FileFragebogenLoader), true);
            RegisterTypeIfMissing(typeof(QuestionnaireViewModel), typeof(QuestionnaireViewModel), true);
            RegisterTypeIfMissing(typeof(MainWindow), typeof(MainWindow), true);
            RegisterTypeIfMissing(typeof(ScoreWindow), typeof(ScoreWindow), true);
            RegisterTypeIfMissing(typeof(FragebogenWindow), typeof(FragebogenWindow), true);
        }
        
        protected override void ConfigureViewModelLocator() {
            BindViewModelToView<QuestionnaireViewModel, MainWindow>();
            BindViewModelToView<QuestionnaireViewModel, ScoreWindow>();
            BindViewModelToView<QuestionnaireViewModel, FragebogenWindow>();
        }

        private void BindViewModelToView<TViewModel, TView>()
        {
            ViewModelLocationProvider.Register(typeof(TView).ToString(), () => Container.TryResolve<TViewModel>());
        }
    }
}