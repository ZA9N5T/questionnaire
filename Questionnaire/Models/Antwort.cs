﻿namespace Questionnaire.Models {
    public class Antwort {
        public const string DONT_KNOW_TEXT = "Dont Know";
        public string Text { get; set; }
        public bool Korrekt { get; set; }
        public bool Ausgewaehlt { get; set; }

        protected bool Equals(Antwort other) {
            return string.Equals(Text, other.Text) && Korrekt == other.Korrekt && Ausgewaehlt == other.Ausgewaehlt;
        }

        /// <inheritdoc />
        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Antwort) obj);
        }

        /// <inheritdoc />
        public override int GetHashCode() {
            unchecked {
                var hashCode = (Text != null ? Text.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Korrekt.GetHashCode();
                hashCode = (hashCode * 397) ^ Ausgewaehlt.GetHashCode();
                return hashCode;
            }
        }
    }
}