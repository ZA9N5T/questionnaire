﻿using System.Collections.Generic;
using System.Linq;

namespace Questionnaire.Models {
    public class Frage {
        public string Text { get; set; }
        public List<Antwort> Antworten { get; set; } = new List<Antwort>();

        protected bool Equals(Frage other) {
            return string.Equals(Text, other.Text) && Antworten.All(antwort => antwort.Equals(other?.Antworten[Antworten.IndexOf(antwort)]));
        }

        public bool IsRichtig()
        {
            return Antworten.All(antwort => antwort.Ausgewaehlt == antwort.Korrekt);
        }

        public bool IsSomethingAusgewählt()
        {
            return Antworten.Any(antwort => antwort.Ausgewaehlt);
        }

        /// <inheritdoc />
        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Frage) obj);
        }

        /// <inheritdoc />
        public override int GetHashCode() {
            return ((Text != null ? Text.GetHashCode() : 0) * 397) ^ (Antworten != null ? Antworten.GetHashCode() : 0);
        }
    }
}