﻿namespace Questionnaire.Models {
    public class QuestionnaireFile {
        public string Name { get; set; }
        public bool Ausgewählt { get; set; }
    }
}